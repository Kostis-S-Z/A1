import java.util.*;

class Player {


	HashMap<Integer, ArrayList<HMM>> hmmsOfSpecies;

	HashMap<Integer, HMM> averageOfBirds;

	static final int numOfSpecies = Constants.COUNT_SPECIES;
	static final int numOfPatterns = Constants.COUNT_SPECIES - 1;
    static final int numOfPossibleMoves = Constants.COUNT_MOVE;

    public Player() {
        hmmsOfSpecies = new HashMap<>();
        for (int i=0; i < numOfSpecies; i++)
        {
            ArrayList<HMM> hmmsOfEachSpecies = new ArrayList<>();
            hmmsOfSpecies.put(i, hmmsOfEachSpecies);
        }
        averageOfBirds = new HashMap<>();
    }


    /**
     * Shoot!
     *
     * This is the function where you start your work.
     *
     * You will receive a variable pState, which contains information about all
     * hmmsOfSpecies, both dead and alive. Each bird contains all past moves.
     *
     * The state also contains the scores for all players and the number of
     * time steps elapsed since the last time this function was called.
     *
     * @param pState the GameState object with observations etc
     * @param pDue time before which we must have returned
     * @return the prediction of a bird we want to shoot at, or cDontShoot to pass
     */
    public Action shoot(GameState pState, Deadline pDue) {
        /*
         * Here you should write your clever algorithms to get the best action.
         * This skeleton never shoots.
         */

        int timestepThreshold = 95;


        if (pState.getRound() > 8) // round 1 and after
        {
            // if there are more than one player then shoot 100 - numofbirds * 2

            for (int i=0; i < numOfSpecies; i++)
            {
                averageOfBirds.put(i, averageHMM(i));  // maybe do this somewhere else
            }

            if (pState.getBird(0).getSeqLength() > timestepThreshold)  // shoot after timestep
            {
                int numOfBirds = pState.getNumBirds();

                double bestGuessOfMovementOfAllBirds = Double.NEGATIVE_INFINITY;
                int bestHMMOfAllBirds = -1;
                int bestBirdGuess = 666;

                for (int i=0; i< numOfBirds; i++)  // for every bird
                {
                    Bird aBird = pState.getBird(i);

                    if (aBird.getLastObservation() != Constants.MOVE_DEAD)  // if the bird is dead you dont care about guessing the species and shooting it
                    {
                        int speciesOfThisBird = findSpecies(aBird);  // guess the species of this bird

                        if (speciesOfThisBird != Constants.SPECIES_BLACK_STORK)   // if the bird is a black stork dont shoot it so no need to guess the movement
                        {
                            // MAXIMUM
                            // TODO: START OF PROBLEM
                            for (int j=0; j < numOfSpecies; j++)  // for every species
                            {
                                //int[] observations = new int[1];
                                //observations[0] = j;
                                int numOfObs = aBird.getSeqLength();

                                int[] observations = new int[numOfObs];
                                for (int ijj=0; ijj < numOfObs; ijj++)
                                {
                                    observations[ijj] = aBird.getObservation(ijj);
                                }

                                int numOfHMMs = hmmsOfSpecies.get(j).size();
                                for (int jj=0; jj < numOfHMMs; jj++)  // for every hmm of the species
                                {
                                    HMM hmmOfABird = hmmsOfSpecies.get(j).get(jj);
                                    double prob = hmmOfABird.a_pass(observations, observations.length);
                                    if (prob > bestGuessOfMovementOfAllBirds)
                                    {
                                        bestGuessOfMovementOfAllBirds = prob;  // TODO: PROBABILITY IS INFINITY
                                        bestHMMOfAllBirds = j;  // j, not jj
                                        bestBirdGuess = i;
                                    }
                                }
                            }
                            // TODO: END OF PROBLEM
                        }
                    }
                }
                if (bestGuessOfMovementOfAllBirds > -120  && bestBirdGuess != 666)  // ???
                {
                    System.err.println("SHOOT" + bestGuessOfMovementOfAllBirds);
                    return new Action(bestBirdGuess, bestHMMOfAllBirds);  // This line would predict that bird 0 will move right and shoot at it.
                }
            }
        }
        return cDontShoot;
    }

    /**
     * Guess the species!
     * This function will be called at the end of each round, to give you
     * a chance to identify the species of the hmmsOfSpecies for extra points.
     *
     * Fill the vector with guesses for the all hmmsOfSpecies.
     * Use SPECIES_UNKNOWN to avoid guessing.
     *
     * @param pState the GameState object with observations etc
     * @param pDue time before which we must have returned
     * @return a vector with guesses for all the hmmsOfSpecies
     */
    public int[] guess(GameState pState, Deadline pDue) {
        /*
         * Here you should write your clever algorithms to guess the species of
         * each bird. This skeleton makes no guesses, better safe than sorry!
         */

        int threshold = 10;  // 1-50 gives same result
        int numOfBirds = pState.getNumBirds();
        int[] lGuess = new int[numOfBirds];

        if (pState.getRound() == 0)  // maybe try guessing pigeons for more rounds -> nope didnt work
        {
            // first round, guess randomly / only pigeons
            for (int i = 0; i < numOfBirds; ++i)
            {
                lGuess[i] = Constants.SPECIES_PIGEON;
            }
        }
        else
        {
            // for every bird find a species
            // System.err.println(numOfBirds);
            for (int i=0; i < numOfBirds; i++)
            {
                if (pState.getBird(i).getSeqLength() > threshold)
                {
                    lGuess[i] = findSpecies(pState.getBird(i));  //TODO: maybe instead of calculating guesses again, use the guess you made when shooting
                }
                else
                {
                    lGuess[i] = Constants.SPECIES_PIGEON;
                }
            }
        }
        return lGuess;
    }

    /**
     *
     *
     * @param bird
     * @return
     */
    public int findSpecies(Bird bird) {

        // get the observations of the current bird
        int numOfObs = bird.getSeqLength();

        int numOfUsefulObs = numOfObs;

        for (int i=0; i < numOfObs; i++)
        {
            if (bird.getObservation(i) == Constants.MOVE_DEAD)
            {
                numOfUsefulObs = i;
                break;
            }
        }

        int[] observations = new int[numOfUsefulObs];
        for (int i=0; i < numOfUsefulObs; i++)
        {
            observations[i] = bird.getObservation(i);
        }

        // for the HMM of each Species (average HMM from the list of HMMs)
        // run an alpha-pass and calculate the probability
        double bestProb = Double.NEGATIVE_INFINITY;
        int bestHMM = Constants.SPECIES_PIGEON;  // if we dont find anything just return pigeon

        // TAKE MAXIMUM OF ALL HMMs

        for (int i=0; i < numOfSpecies; i++)
        {
            int numOfHMMs = hmmsOfSpecies.get(i).size();
            for (int j=0; j < numOfHMMs; j++)
            {
                HMM hmmOfABird = hmmsOfSpecies.get(i).get(j);
                double prob = hmmOfABird.a_pass(observations, observations.length);
                if (prob > bestProb)
                {
                    bestProb = prob;
                    bestHMM = i;
                }
            }
        }
        // return the HMM that had the highest probability
        return bestHMM;
    }

    /**
     * If you hit the bird you were trying to shoot, you will be notified
     * through this function.
     *
     * @param pState the GameState object with observations etc
     * @param pBird the bird you hit
     * @param pDue time before which we must have returned
     */
    public void hit(GameState pState, int pBird, Deadline pDue) {
        System.err.println("HIT BIRD!!!");
    }

    /**
     * If you made any guesses, you will find out the true species of those
     * hmmsOfSpecies through this function.
     *
     * @param pState the GameState object with observations etc
     * @param pSpecies the vector with species
     * @param pDue time before which we must have returned
     */
    public void reveal(GameState pState, int[] pSpecies, Deadline pDue) {

        // the HMM of the bird is calculated by Baum Welch
        // get the correct species of this bird
        // put the HMM of the bird to the corresponding column in the matrix

        // Each round the are many hmmsOfSpecies
        // For each bird get the observations of the current round
        for (int i=0; i < pState.getNumBirds(); i++)
        {
            if (pSpecies[i] != Constants.SPECIES_UNKNOWN)
            {
                Bird bird = pState.getBird(i);
                // get the observations of the current bird
                int numOfObs = bird.getSeqLength();

                int numOfUsefulObs = numOfObs;

                for (int j=0; j < numOfObs; j++)
                {
                    if (bird.getObservation(j) == Constants.MOVE_DEAD)
                    {
                        numOfUsefulObs = j;
                        break;
                    }
                }

                int[] observations = new int[numOfUsefulObs];
                for (int j=0; j < numOfUsefulObs; j++)
                {
                    observations[j] = bird.getObservation(j);
                }


                HMM hmmOfABird = new HMM(numOfPatterns, numOfPossibleMoves);  // create a new HMM and based on the observations of the current bird train it

                hmmOfABird.initializeRandomly();  // initiliaze "randomly"

                hmmOfABird.BaumWelch(observations);  // train our hmm with the observation sequence we got

                int speciesNumber = pSpecies[i];  // get the correct species

                hmmsOfSpecies.get(speciesNumber).add(hmmOfABird);  // add the trained HMM to the list of HMMs of the corresponding species
            }
        }
    }

    public static final Action cDontShoot = new Action(-1, -1);

    public HMM averageHMM(int indexOfSpecies)
    {
        HMM averageOfAnHMM = new HMM(numOfPatterns, numOfPossibleMoves);

        double[][] dataA = new double[numOfPatterns][numOfPatterns];
        double[][] dataB = new double[numOfPatterns][numOfPossibleMoves];
        double[][] dataPi = new double[1][numOfPatterns];

        int ddd = hmmsOfSpecies.get(indexOfSpecies).size();
        for (int d=0; d < ddd; d++)
        {
            Matrix aMatrixCurrent = hmmsOfSpecies.get(indexOfSpecies).get(d).A;
            for (int n = 0; n < numOfPatterns; n++)
            {
                for (int m = 0; m < numOfPatterns; m++)
                {
                    dataA[n][m] += aMatrixCurrent.getElement(n,m);
                }
            }

            Matrix bMatrixCurrent = hmmsOfSpecies.get(indexOfSpecies).get(d).B;
            for (int n = 0; n < numOfPatterns; n++)
            {
                for (int m=0; m < numOfPossibleMoves; m++)
                {
                    dataB[n][m] += bMatrixCurrent.getElement(n,m);
                }
            }

            Matrix pMatrixCurrent = hmmsOfSpecies.get(indexOfSpecies).get(d).pi;
            for (int n = 0; n < numOfPatterns; n++)
            {
                dataPi[0][n] += pMatrixCurrent.getElement(0, n);
            }
        }

        for (int n = 0; n < numOfPatterns; n++)
        {
            for (int m = 0; m < numOfPatterns; m++)
            {
                dataA[n][m] = dataA[n][m] / (double) ddd;
            }
        }

        for (int n = 0; n < numOfPatterns; n++)
        {
            for (int m=0; m < numOfPossibleMoves; m++)
            {
                dataB[n][m] = dataB[n][m] / (double) ddd;
            }
        }

        for (int n = 0; n < numOfPatterns; n++)
        {
            dataPi[0][n] = dataA[0][n] / (double) ddd;
        }

        Matrix aAverage = new Matrix(dataA);
        Matrix bAverage = new Matrix(dataB);
        Matrix piAverage = new Matrix(dataPi);


        averageOfAnHMM.A = aAverage;
        averageOfAnHMM.B = bAverage;
        averageOfAnHMM.pi = piAverage;

        return averageOfAnHMM;
    }

}
