import java.util.*;

class Player {


	HashMap<Integer, ArrayList<HMM>> hmmsOfSpecies;

	HashMap<Integer, HMM> averageOfBirds;

	static final int numOfSpecies = Constants.COUNT_SPECIES - 1;
    static final int numOfPossibleMoves = Constants.COUNT_MOVE;

    public Player() {
        hmmsOfSpecies = new HashMap<>();
        for (int i=0; i < Constants.COUNT_SPECIES; i++)
        {
            ArrayList<HMM> hmmsOfEachSpecies = new ArrayList<>();
            hmmsOfSpecies.put(i, hmmsOfEachSpecies);
        }
        averageOfBirds = new HashMap<>();
    }


    /**
     * Shoot!
     *
     * This is the function where you start your work.
     *
     * You will receive a variable pState, which contains information about all
     * hmmsOfSpecies, both dead and alive. Each bird contains all past moves.
     *
     * The state also contains the scores for all players and the number of
     * time steps elapsed since the last time this function was called.
     *
     * @param pState the GameState object with observations etc
     * @param pDue time before which we must have returned
     * @return the prediction of a bird we want to shoot at, or cDontShoot to pass
     */
    public Action shoot(GameState pState, Deadline pDue) {
        /*
         * Here you should write your clever algorithms to get the best action.
         * This skeleton never shoots.
         */

        if (pState.getRound() == 0)
        {
            return cDontShoot;
        }
        else
        {
            for (int i=0; i < Constants.COUNT_SPECIES; i++)
            {

                averageOfBirds.put(i, averageHMM(i));
            }

            int numOfBirds = pState.getNumBirds();

            for (int i=0; i<numOfBirds; i++)
            {
                Bird aBird = pState.getBird(i);
            }

            // viterbi

            //int[] predictions = guess(pState, pDue);

            // if there are more than one player then shoot 100 - numofbirds * 2
            return cDontShoot;
            // This line would predict that bird 0 will move right and shoot at it.
            // return Action(0, MOVE_RIGHT);
        }
    }


    /**
     * Guess the species!
     * This function will be called at the end of each round, to give you
     * a chance to identify the species of the hmmsOfSpecies for extra points.
     *
     * Fill the vector with guesses for the all hmmsOfSpecies.
     * Use SPECIES_UNKNOWN to avoid guessing.
     *
     * @param pState the GameState object with observations etc
     * @param pDue time before which we must have returned
     * @return a vector with guesses for all the hmmsOfSpecies
     */
    public int[] guess(GameState pState, Deadline pDue) {
        /*
         * Here you should write your clever algorithms to guess the species of
         * each bird. This skeleton makes no guesses, better safe than sorry!
         */

        int numOfBirds = pState.getNumBirds();
        int[] lGuess = new int[numOfBirds];

        if (pState.getRound() == 0)  // maybe try guessing pigeons for more rounds
        {
            // first round, guess randomly / only pigeons
            for (int i = 0; i < numOfBirds; ++i)
            {
                lGuess[i] = Constants.SPECIES_PIGEON;
            }
        }
        else
        {
            // for every bird find a species
            // System.err.println(numOfBirds);
            for (int i=0; i < numOfBirds; i++)
            {
                if (pState.getBird(i).getSeqLength() > 10)
                {
                    lGuess[i] = findSpecies(pState.getBird(i));  //TODO: maybe instead of calculating guesses again, use the guess you made when shooting
                }
                else
                {
                    lGuess[i] = Constants.SPECIES_PIGEON;
                }
            }
        }
        return lGuess;
    }

    /**
     *
     *
     * @param bird
     * @return
     */
    public int findSpecies(Bird bird) {

        // get the observations of the current bird
        int numOfObs = bird.getSeqLength();

        int numOfUsefulObs = 0;
        for (int j=0; j < numOfObs; j++)
        {
            if (bird.getObservation(j) == Constants.MOVE_DEAD)
            {
                numOfUsefulObs = j;
                break;
            }
        }

        int[] observations = new int[numOfObs];
        for (int j=0; j < numOfUsefulObs; j++)
        {
            observations[j] = bird.getObservation(j);
        }

        // for the HMM of each Species (average HMM from the list of HMMs)
        // run an alpha-pass and calculate the probability
        double bestProb = Double.NEGATIVE_INFINITY;
        int bestHMM = -1;

        // TAKE MAXIMUM OF ALL HMMs
        /*
        for (int i=0; i < Constants.COUNT_SPECIES; i++)
        {
            int numOfHMMs = hmmsOfSpecies.get(i).size();
            for (int j=0; j < numOfHMMs; j++)
            {
                HMM hmmOfABird = hmmsOfSpecies.get(i).get(j);
                double prob = hmmOfABird.a_pass(observations, observations.length);
                if (prob > bestProb)
                {
                    bestProb = prob;
                    bestHMM = i;
                }
            }
        }
        */
        //  TAKE PROBABILITY OF AVERAGE

        for (int i=0; i < Constants.COUNT_SPECIES; i++)
        {
            HMM hmmOfABird = averageOfBirds.get(i);
            double prob = hmmOfABird.a_pass(observations, observations.length);
            if (prob > bestProb)
            {
                bestProb = prob;
                bestHMM = i;
            }
        }
        // return the HMM that had the highest probability
        return bestHMM;
    }

    /**
     * If you hit the bird you were trying to shoot, you will be notified
     * through this function.
     *
     * @param pState the GameState object with observations etc
     * @param pBird the bird you hit
     * @param pDue time before which we must have returned
     */
    public void hit(GameState pState, int pBird, Deadline pDue) {
        System.err.println("HIT BIRD!!!");
    }

    /**
     * If you made any guesses, you will find out the true species of those
     * hmmsOfSpecies through this function.
     *
     * @param pState the GameState object with observations etc
     * @param pSpecies the vector with species
     * @param pDue time before which we must have returned
     */
    public void reveal(GameState pState, int[] pSpecies, Deadline pDue) {

        // the HMM of the bird is calculated by Baum Welch
        // get the correct species of this bird
        // put the HMM of the bird to the corresponding column in the matrix

        // Each round the are many hmmsOfSpecies
        // For each bird get the observations of the current round
        for (int i=0; i < pState.getNumBirds(); i++)
        {
            if (pSpecies[i] != Constants.SPECIES_UNKNOWN)
            {
                Bird bird = pState.getBird(i);
                int numOfObs = bird.getSeqLength();

                // if the bird dies then its observations are no longer useful
                // take the observations for the bird until it was alive
                int numOfUsefulObs = 0;
                for (int j=0; j < numOfObs; j++)
                {
                    if (bird.getObservation(j) == Constants.MOVE_DEAD)
                    {
                        numOfUsefulObs = j;
                        break;
                    }
                }

                int[] observations = new int[numOfObs];
                for (int j=0; j < numOfUsefulObs; j++)
                {
                    observations[j] = bird.getObservation(j);
                }

                HMM hmmOfABird = new HMM(numOfSpecies, numOfPossibleMoves);  // create a new HMM and based on the observations of the current bird train it

                hmmOfABird.initializeRandomly();  // initiliaze "randomly"

                hmmOfABird.BaumWelch(observations);  // train our hmm with the observation sequence we got

                int speciesNumber = pSpecies[i];  // get the correct species

                hmmsOfSpecies.get(speciesNumber).add(hmmOfABird);  // add the trained HMM to the list of HMMs of the corresponding species
            }
        }
    }

    public static final Action cDontShoot = new Action(-1, -1);

    public HMM averageHMM(int indexOfSpecies)
    {
        HMM averageOfAnHMM = new HMM(numOfSpecies, numOfPossibleMoves);

        double[][] dataA = new double[numOfSpecies][numOfSpecies];
        double[][] dataB = new double[numOfSpecies][numOfPossibleMoves];
        double[][] dataPi = new double[1][numOfSpecies];

        int ddd = hmmsOfSpecies.get(indexOfSpecies).size();
        for (int d=0; d < ddd; d++)
        {
            Matrix aMatrixCurrent = hmmsOfSpecies.get(indexOfSpecies).get(d).A;
            for (int n=0; n < numOfSpecies; n++)
            {
                for (int m=0; m < numOfSpecies; m++)
                {
                    dataA[n][m] += aMatrixCurrent.getElement(n,m);
                }
            }

            Matrix bMatrixCurrent = hmmsOfSpecies.get(indexOfSpecies).get(d).B;
            for (int n=0; n < numOfSpecies; n++)
            {
                for (int m=0; m < numOfPossibleMoves; m++)
                {
                    dataB[n][m] += bMatrixCurrent.getElement(n,m);
                }
            }

            Matrix pMatrixCurrent = hmmsOfSpecies.get(indexOfSpecies).get(d).pi;
            for (int n=0; n < numOfSpecies; n++)
            {
                dataPi[0][n] += pMatrixCurrent.getElement(0, n);
            }
        }

        for (int n=0; n < numOfSpecies; n++)
        {
            for (int m=0; m < numOfSpecies; m++)
            {
                dataA[n][m] = dataA[n][m] / (double) ddd;
            }
        }

        for (int n=0; n < numOfSpecies; n++)
        {
            for (int m=0; m < numOfPossibleMoves; m++)
            {
                dataB[n][m] = dataB[n][m] / (double) ddd;
            }
        }

        for (int n=0; n < numOfSpecies; n++)
        {
            dataPi[0][n] = dataA[0][n] / (double) ddd;
        }

        Matrix aAverage = new Matrix(dataA);
        Matrix bAverage = new Matrix(dataB);
        Matrix piAverage = new Matrix(dataPi);


        averageOfAnHMM.A = aAverage;
        averageOfAnHMM.B = bAverage;
        averageOfAnHMM.pi = piAverage;

        return averageOfAnHMM;
    }

}
