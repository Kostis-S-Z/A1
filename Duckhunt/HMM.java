import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class HMM {

    public Matrix A;
    public Matrix B;
    public Matrix pi;
    public Matrix observations_as_mat = null;

    // delta pass (veterbi Alg)
    public int p_len;

    // a_pass
    public Matrix alpha = null;
    public Matrix c_t = null;

    // b_pass
    public Matrix beta = null;

    // gammas
    public Matrix gamma = null;
    public List<Matrix> di_gammas = null;

    public int N;
    public int M;

    public double oldLogProb = Double.NEGATIVE_INFINITY;
    public int iters = 0;
    public int maxIters = 100;

    public HMM(int N, int M) {
        this.N = N;
        this.M = M;

        // Initialize A

        // Initialize B

        // Initialize pi
    }

    public void BaumWelch(int[] observations) {
        int T = observations.length;
        iters = iters + 1;
        double logProb;

        while (iters < maxIters) {
            a_pass(observations, T);
            b_pass(observations, T);
            gammas(observations, T);
            estimate(observations, T);
            logProb = compute_log(T);
            if (logProb > oldLogProb) {
                iters += 1;
                oldLogProb = logProb;
            } else {
                break;
            }
        }
    }

    public double a_pass(int[] observations, int T) {
        int obs_t = observations[0];

        double c0 = 0.0;
        alpha = new Matrix(N, T);
        c_t = new Matrix(T, 1);

        Matrix alpha_0 = new Matrix(N, 1);
        for (int i = 0; i < N; i++) {
            double elem = pi.getElement(0, i) * B.getElement(i, obs_t);
            alpha_0.setElement(elem, i, 0);
            c0 += elem;
        }
        c_t.setElement(c0, 0, 0);
        alpha_0 = scale(c0, alpha_0, N);

        alpha.setColumn(alpha_0, 0);
        Matrix alpha_t = null;
        double ct = 0.0;
        for (int t = 1; t < T; t++)
        {
            ct = 0.0;
            obs_t = observations[t];
            alpha_t = new Matrix(N, 1);
            for (int i = 0; i < N; i++)
            {
                double a_t_i = 0.0;

                for (int j = 0; j < N; j++)
                {
                    a_t_i = a_t_i + alpha.getElement(j, t - 1) * A.getElement(j, i);  // t-1 or obs_t-1?
                }
                a_t_i = a_t_i * B.getElement(i, obs_t);
                alpha_t.setElement(a_t_i, i, 0);
                ct = ct + a_t_i;
            }
            ct = 1.0 / ct;
            c_t.setElement(ct, t, 0);
            alpha_t = scale(ct, alpha_t, N);
            alpha.setColumn(alpha_t, t);
        }
        double output = 0;
        for (int i = 0; i < T; i++) {

            output += Math.log(c_t.getElement(i, 0));
            //output += alpha.getElement(i, T - 1);
        }
        return -output;
    }

    public void b_pass(int[] observations, int T) {
        int obs_t = T - 1; // observations[T-1];

        beta = new Matrix(N, T);
        for (int i = 0; i < N; i++) {
            beta.setElement(c_t.getElement(obs_t, 0), i, obs_t);  // get 999 c_T so it is T-1 NOT obs_t
        }

        Matrix beta_t = null;
        for (int t = T - 2; t > -1; t--) {
            obs_t = t; // observations[t];
            beta_t = new Matrix(N, 1);
            for (int i = 0; i < N; i++) {
                double b_t_i = 0.0;
                int obs_t_1 = observations[t + 1];
                for (int j = 0; j < N; j++) {
                    b_t_i = b_t_i + A.getElement(i, j) * B.getElement(j, obs_t_1) * beta.getElement(j, t + 1);  // obs_t+1 or T+1 ???
                }
                b_t_i = c_t.getElement(t, 0) * b_t_i;
                beta_t.setElement(b_t_i, i, 0);
            }
            beta.setColumn(beta_t, t);
        }
    }

    public void gammas(int[] observations, int T) {
        di_gammas = new ArrayList<>();
        gamma = new Matrix(N, T);

        for (int t = 0; t < T - 1; t++) {
            double denom = 0.0;
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < N; j++) {
                    double alpha_t_i = alpha.getElement(i, t);

                    double a_i_j = A.getElement(i, j);

                    int obs_t_1 = observations[t + 1];
                    double b_j_O_t_1 = B.getElement(j, obs_t_1);  // maaaaaaaaybe obs_t_1, j

                    double beta_t_1 = beta.getElement(j, t + 1); // maaaaybe obs_t_1, j ORRR j, t+1

                    denom = denom + alpha_t_i * a_i_j * b_j_O_t_1 * beta_t_1;
                }
            }

            Matrix gamma_t = new Matrix(N, 1);
            Matrix di_gamma_t = new Matrix(N, N);
            for (int i = 0; i < N; i++) {
                double gamma_t_i = 0.0;
                for (int j = 0; j < N; j++) {
                    double alpha_t_i = alpha.getElement(i, t);

                    double a_i_j = A.getElement(i, j);

                    int obs_t_1 = observations[t + 1];
                    double b_j_O_t_1 = B.getElement(j, obs_t_1);  // maaaaaaaaybe obs_t_1, j

                    double beta_t_1 = beta.getElement(j, t + 1); // maaaaybe obs_t_1, j ORRR j, t+1

                    double res = alpha_t_i * a_i_j * b_j_O_t_1 * beta_t_1;

                    double di_gamma_i_t = res / denom;

                    di_gamma_t.setElement(di_gamma_i_t, i, j);

                    gamma_t_i = gamma_t_i + di_gamma_i_t;
                }
                gamma_t.setElement(gamma_t_i, i, 0);
                gamma.setColumn(gamma_t, t);
            }
            di_gammas.add(di_gamma_t);
        }

        double denom = 0;
        for (int i = 0; i < N; i++) {
            denom = denom + alpha.getElement(i, T - 1);
        }
        for (int i = 0; i < N; i++) {
            double elem = alpha.getElement(i, T - 1) / denom;
            gamma.setElement(elem, i, T - 1);
        }
    }

    public void estimate(int[] observations, int T) {
        // re-estimate pi
        for (int i = 0; i < N; i++) {
            double g_0_i = gamma.getElement(i, 0);
            pi.setElement(g_0_i, 0, i);
        }

        // re-estimate A
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                double numer = 0.0;
                double denom = 0.0;
                for (int t = 0; t < T - 1; t++) {
                    numer = numer + di_gammas.get(t).getElement(i, j);
                    denom = denom + gamma.getElement(i, t);
                }
                double elem = numer / denom;
                A.setElement(elem, i, j);
            }
        }

        // re-estimate B
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                double numer = 0.0;
                double denom = 0.0;
                for (int t = 0; t < T; t++) {
                    int obs_t = observations[t];
                    if (obs_t == j) {
                        numer = numer + gamma.getElement(i, t);
                    }
                    denom = denom + gamma.getElement(i, t);
                }
                double elem = numer / denom;
                B.setElement(elem, i, j);  // fixed j,i to i,j
            }
        }

    }

    public double compute_log(int T) {
        double logProb = 0.0;
        for (int i = 0; i < T - 1; i++) {
            logProb = logProb + Math.log(c_t.getElement(i, 0));
        }
        logProb = -logProb;
        return logProb;
    }

    public Matrix scale(double scaler, Matrix alpha_i, int N) {
        Matrix alpha_i_scaled = new Matrix(N, 1);

        for (int i = 0; i < N; i++) {
            double elem_scaled = scaler * alpha_i.getElement(i, 0);
            alpha_i_scaled.setElement(elem_scaled, i, 0);
        }

        return alpha_i_scaled;
    }

    public Matrix viterbi() {
        p_len = pi.columns;
        Matrix init_delta = pi.elemMult(B.getColumn((int) observations_as_mat.object[0][0]));
        Matrix delta = new Matrix(p_len, N);
        Matrix delta_max_idx = new Matrix(p_len, p_len * (N - 1));
        delta.setColumn(init_delta, 0); //*

        for (int i = 1; i < N; i++) // because of *
        {
            Matrix last_delta_mat = delta.getColumn(i - 1).transpose().repmat(p_len, 1);
            Matrix first_elem_mult = last_delta_mat.elemMult(A.transpose());
            Matrix cur_Obs_Mat = B.getColumn((int) observations_as_mat.object[0][i]).repmat(1, p_len);
            Matrix second_elem_mult = first_elem_mult.elemMult(cur_Obs_Mat);
            Matrix max_rows = second_elem_mult.maxValRows();

            // fix for the probability = zero value special case!
            for (int n = 0; n < p_len; n++) {
                if (max_rows.object[n][0] == 0) {
                    for (int m = 1; m < p_len + 1; m++) {
                        max_rows.object[n][m] = 0;
                    }
                }
            }

            for (int n = 0; n < p_len; n++)
                delta.object[n][i] = max_rows.object[n][0];
            for (int m = 0; m < A.columns; m++) {
                for (int n = 0; n < p_len; n++) {
                    delta_max_idx.object[n][(i - 1) * p_len + m] = max_rows.object[n][m + 1];
                }
            }
        }
        Matrix last_column = new Matrix(p_len, 1);
        last_column = last_column.getColumnPart(delta, N - 1, 0, p_len - 1).transpose();
        Matrix max_last_column = last_column.maxValRows();

        int col = 0;
        boolean flag = false;
        Matrix X_Seq = new Matrix(N, 1);

        for (int m = 1; m < max_last_column.columns; m++) {
            if (max_last_column.object[0][m] == 1) {
                if (flag == true) {
                    X_Seq = X_Seq.addColumn();
                    col = col + 1;
                    X_Seq.object[0][col] = m;
                } else
                    X_Seq.object[0][col] = m;
                flag = true;
            }
        }

        int length = N;
        boolean flag2 = false;
        int row_state = 0;

        X_Seq = Possible_Pathes(X_Seq, delta_max_idx, p_len, length, row_state, flag2);
        //X_Seq.showMatrix();
        // adjust Output to Kattis
        //for (int n = 0; n < X_Seq.rows; n++){
        //    int value = (int) X_Seq.object[X_Seq.rows-1 - n][0]-1;
        //    System.out.printf("%d ", value);
        //}
        return X_Seq;
    }

    public Matrix Possible_Pathes(Matrix X_Seq, Matrix delta_max_idx, int p_length, int length, int row_state, boolean flag) {
        int T = X_Seq.columns;
        int zeile;
        int spalte;
        Matrix tmp_col_vec;
        for (int t = 0; t < T; t++) {
            for (int m = 0; m < p_length; m++) {
                zeile = (int) X_Seq.object[row_state][t] - 1;
                spalte = (length - 1) * p_length - m - 1;
                if (delta_max_idx.object[zeile][spalte] == 1) {
                    if (flag == false) {
                        X_Seq.object[row_state + 1][t] = p_length - m;
                        flag = true;
                    } else {
                        X_Seq = X_Seq.addColumn();
                        tmp_col_vec = X_Seq.getColumn(t);
                        X_Seq = X_Seq.setColumn(tmp_col_vec, X_Seq.columns - 1);
                        X_Seq.object[row_state + 1][X_Seq.columns - 1] = p_length - m;
                        if (m == p_length - 1) {
                            flag = false;
                        }
                    }
                }
            }
            flag = false;
        }
        row_state = row_state + 1;
        length = length - 1;
        if (length == 1) {
            return X_Seq;
        } else {
            return Possible_Pathes(X_Seq, delta_max_idx, p_length, length, row_state, flag);
        }
    }

    public void initializeRandomly()
    {
        Matrix initA = new Matrix(N,N);

        for (int n=0; n < N; n++)
        {
            initA.object[n][n] = 1.0;
        }
        this.A = initA;

        double[][] dataB = new double[N][M];
        Random r = new Random();
        for (int mi=0; mi < N; mi++)
        {
            double sum = 0.0;
            for (int mj=0; mj < M; mj++)
            {
                dataB[mi][mj] = (1.0 / M) + Math.pow (-1, mi) * (1.0 / M) * 0.0 + (1.0 - 0.0) * r.nextDouble();
                sum += dataB[mi][mj];
            }

            for (int mj=0; mj < M; mj++)
            {
                dataB[mi][mj] = dataB[mi][mj] / sum;
            }
        }

        double[][] dataP = new double[1][N];

        for (int mj=0; mj < N; mj++)
        {
            dataP[0][mj] = 0.2; //0.05 + (0.95 - 0.05) * r.nextDouble();
        }

        this.B = new Matrix(dataB);
        this.B.showMatrix();
        this.pi = new Matrix(dataP);
    }
}