import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;


public class HMM0 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

		double[][] t_mat = null;
		double[][] o_mat = null;
		double[][] init_state = null;

        int lines = 0;
        do
        {
        	lines += 1;
        	String input = sc.nextLine();
        	String[] splitted = input.split("\\s+");
        	int n = Integer.parseInt(splitted[0]);  // rows
        	int m = Integer.parseInt(splitted[1]);  // columns
        	int k = 2;
        	switch (lines)
        	{
        		case 1:
        			t_mat = parse_matrix(Arrays.copyOfRange(splitted, 2, splitted.length) , n, m); 
        			break;
        		case 2:
        			o_mat = parse_matrix(Arrays.copyOfRange(splitted, 2, splitted.length) , n, m);
        			break;
        		case 3:        			
        			init_state = parse_matrix(Arrays.copyOfRange(splitted, 2, splitted.length) , n, m); 
        			break;
        		default:
        			System.err.println("Too many lines / matrices");
        	}
        }while (sc.hasNextLine());
        
        //System.out.println("Observation matrix:");
        //show_matrix(o_mat);
        //System.out.println("Initial State:");
        //show_matrix(init_state);

        //System.out.println("Start! initial state * transition matrix");
		// Given this knowledge, we can compute the probability of observing each observation. 
		double[][] current_state = matrix_mul(init_state, t_mat);
		//show_matrix(current_state);

        //System.out.println("Continue! current state * observation matrix");
        // First, we need to multiply the transition matrix with our current estimate of states.
		double[][] state_probs = matrix_mul(current_state,o_mat);
		//show_matrix(state_probs);
		
		String output = new String();
		
		output += Integer.toString(state_probs.length);
		output += " ";
		output += Integer.toString(state_probs[0].length);

		int rows = state_probs.length;
		int cols = state_probs[0].length;
		for (int i=0; i < rows; i++)
		{
			for (int j=0; j < cols; j++)
			{
				output += " ";
				output += state_probs[i][j];
			}
		}
		
		System.out.println(output);
	}
	
	public static double[][] parse_matrix(String[] input, int rows, int columns)
	{
		double[][] matrix = new double[rows][columns];
		int k=0;
		for (int i=0; i < rows; i++)
		{
			for (int j=0; j < columns; j++)
			{
				matrix[i][j] = Double.parseDouble(input[k]);
				k += 1;
			}
		}
		return matrix;
	}
	
	public static void show_matrix(double[][] matrix)
	{		

		int rows = matrix.length;
		int cols = matrix[0].length;
		for (int i=0; i<rows; i++)
		{
			String print_row = new String();
			for (int j=0; j<cols; j++)
			{
				print_row += matrix[i][j] + " ";
			}
			System.out.println(print_row);
		}
	}
	

	public static double[][] transpose(double[][] matrix)
	{
		int rows = matrix.length;
		int cols = matrix[0].length;
		double[][] transposed = new double[cols][rows];
		for (int i=0; i < rows; i++)
		{
			for (int j=0; j < cols; j++)
			{
				transposed[j][i] = matrix[i][j];
			}
		}
		return transposed;
	}

	
	public static double[][] elem_mul(double[][] X, double[][] Y)
	{
		int rows = X.length;
		int cols = X[0].length;
		int rows2 = Y.length;
		int cols2 = Y[0].length;
		
		double[][] result = X;
		
		if (rows != rows2 || cols != cols2)
		{
			System.out.println("Wrong dimensions!");
		}
		
		for (int i=0; i < rows; i++)
		{
			for (int j=0; j < cols; j++)
			{
				result[i][j] *= Y[i][j];
			}
		}
		return result;
	}
	
	public static double[][] matrix_mul(double[][] X, double[][] Y)
	/*
	 * Matrix Multiplication
	 */
	{
		
		int rows = X.length;
		int cols = X[0].length;
		int rows2 = Y.length;
		int cols2 = Y[0].length;
		
		if (rows2 != cols)
		{
			System.out.println("Wrong dimensions");
		}
		
		double[][] result = new double[rows][cols2];
		
		for (int i=0; i < rows; i++)
		{
			for (int j=0; j < cols2; j++)
			{
				for (int k=0; k < rows2; k++)
				{
					result[i][j] += X[i][k] * Y[k][j];
				}
			}
		}
		return result;
	}

}
