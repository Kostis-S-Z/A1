public class Matrix {

    public int rows;
    public int columns;
    public double[][] object;

    // create vec or mat
    // public matrix()
    public Matrix(double[][] data) {
        this.rows = data.length;
        this.columns = data[0].length;
        this.object = new double[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++)
                object[i][j] = data[i][j];
        }
    }

    // overload the method matrix to create empty matrices of a desired size
    public Matrix(int row, int col) {
        this.rows = row;
        this.columns = col;
        this.object = new double[rows][columns];
    }

    // Method to print the mat
    public void show_mat() {
        for (int i = 0; i < rows; i++) {
            String print_row = "";
            for (int j = 0; j < columns; j++) {
                print_row = print_row + object[i][j] + " ";
            }
            System.out.println(print_row);
        }
    }
    
    public void setElement(double elem, int row, int col)
    {
    	this.object[row][col] = elem;
    }
    
    public double getElement(int row, int col)
    {
    	return this.object[row][col];
    }

    public int number_of_rows(){
        return this.rows;
    }

    public int number_of_columns(){
        return this.columns;
    }

  // METHOD TO MULTIPLY MATRICES.
    public Matrix mat_multiply_with(Matrix B){
        Matrix A = this;
        if (A.columns != B.rows)
            throw new RuntimeException("Multiplication not possible... check rows and columns of the matrices!");
        Matrix Result = new Matrix(A.number_of_rows(),B.number_of_columns());
        for (int i = 0; i < Result.rows; i++)
            for (int j = 0; j < Result.columns; j++)
                for (int k = 0; k < A.columns; k++)
                    Result.object[i][j] = Result.object[i][j] + (A.object[i][k] * B.object[k][j]);
        return Result;
   }

   // Method to get a row of a Matrix
    // it is redundant to transpose a matrix and than use get_column, so it is not really needed and just luxus.
    public Matrix get_row( int row_idx){
        Matrix row_vec = new Matrix( 1,  this.number_of_columns());
        for(int i = 0; i< row_vec.number_of_columns(); i++)
            row_vec.object[0][i] = this.object[row_idx][i];
        return row_vec;
    }

    // Method to get a column of a matrix
    public Matrix get_column( int column_idx){
        Matrix column_vec = new Matrix(this.number_of_rows(), 1);
        for (int i = 0; i< column_vec.number_of_rows(); i++)
            column_vec.object[i][0] = this.object[i][column_idx];
        return column_vec;
    }

    // method to do elementwise multiplication
    public Matrix elementwise_multipy_with(Matrix B){
        Matrix A = this;
        if (A.columns != B.columns || A.rows != B.rows)
            throw new RuntimeException("For pointwise multiplication the sizes of the matrices must be equal");
        Matrix Result =  new Matrix(A.number_of_rows(),B.number_of_columns());
        for (int i = 0; i< Result.rows; i++)
            for(int j = 0; j<Result.columns; j++)
                Result.object[i][j] = A.object[i][j] * B.object[i][j];
        return Result;

    }

    public Matrix transpose(){
        Matrix transposed = new Matrix(this.number_of_columns(),this.number_of_rows());
        for(int n = 0; n<this.number_of_rows(); n++)
            for(int m = 0; m<number_of_columns(); m++ )
                transposed.object[m][n] = this.object[n][m];
        return transposed;
    }

    public Matrix repmat(int y_way, int x_way){
        // create output matrix (empty)
        Matrix rep_result = new Matrix(this.rows * y_way, this.columns*x_way);
            for (int y = 0; y < y_way; y++)
                for (int n = 0; n < this.rows; n++)
                    for (int x = 0; x < x_way; x++)
                        for(int m = 0; m < this.columns;m++)
                            rep_result.object[y*this.rows + n][x*this.columns + m] = this.object[n][m];
        return rep_result;
    }

    // a method which can write a vector to a matrix like: "alpha_Mat(:,1) = vector"
    public Matrix set_column_into_matrix(Matrix vec, int col){
        for (int n=0; n<this.rows; n++)
            this.object[n][col] = vec.object[n][0];
        return this;
    }

    // redundant to "get_column" method!
    /*public matrix get_column_from_matrix(matrix mat, int col){
        for(int n=0; n<mat.rows; n++)
            this.object[n][0]= mat.object[n][col];
        return this;
    }
    */

    // a method which add a column at the end of a matrix
    public Matrix add_column(){
        Matrix new_mat = new Matrix(this.rows, this.columns+1);
        for(int m=0; m<this.columns; m++) {
            Matrix current_column = this.get_column(m);
            new_mat.set_column_into_matrix(current_column,m);
        }
        return new_mat;
    }

    // a method which find the max values in a row and its index. It will only return the last max index!
    public Matrix max_val_rows(){
        boolean are_multiple_max=false;
        Matrix max_of_row_val_vec = new Matrix(this.rows,1);
        Matrix column_idx_of_max_in_row = new Matrix(this.rows,1);
        double current_max_in_row;



        for (int n = 0; n < this.rows; n++) {
            // value stuff
            current_max_in_row = this.object[n][0];

            // Index stuff
            double[][] dummy = {{0}};
            Matrix current_index_of_max_in_row = new Matrix(dummy);
            System.out.println("current_index_of_max_in_row:");
            current_index_of_max_in_row.show_mat();
            System.out.printf("n is %d", n);

            for (int m = 1; m < this.columns; m++) {
                System.out.printf("this is variable m %d", m);
                if (this.object[n][m] > current_max_in_row) {
                    // Value stuff
                    current_max_in_row = this.object[n][m];

                    //Index Stuff
                    current_index_of_max_in_row.object[0][0] = m;

                    System.out.println("current_index_of_max_in_row:");
                    current_index_of_max_in_row.show_mat();

                    if (are_multiple_max ==true) {
                        current_index_of_max_in_row = new Matrix(dummy);
                        current_index_of_max_in_row.object[0][0]=m;

                        System.out.println("current_index_of_max_in_row:");
                        current_index_of_max_in_row.show_mat();

                        are_multiple_max =false;
                    }
                }
                else if(this.object[n][m] == current_max_in_row){
                    current_index_of_max_in_row = current_index_of_max_in_row.add_column();
                    current_index_of_max_in_row.object[0][this.rows]=m;

                    System.out.println("current_index_of_max_in_row:");
                    current_index_of_max_in_row.show_mat();

                    are_multiple_max = true;
                }
            }
            max_of_row_val_vec.object[n][0] = current_max_in_row;
            if (are_multiple_max){
                for(int k=0; k< current_index_of_max_in_row.columns; k++) {
                    column_idx_of_max_in_row = column_idx_of_max_in_row.add_column();
                    column_idx_of_max_in_row.object[n][k] = current_index_of_max_in_row.object[0][k];
                    are_multiple_max = false;
                }
            }

        }
        Matrix Output = new Matrix(this.rows,column_idx_of_max_in_row.columns + 1);
        Output = Output.set_column_into_matrix(max_of_row_val_vec,0);
        for(int t=1; t < column_idx_of_max_in_row.columns + 1; t++)
            Output = Output.set_column_into_matrix(column_idx_of_max_in_row,t);
        return Output;
    }

    //}

    // size function like in python (Todo)
    /*public int[2] size(){
         int[] a = new int[2];
         a[0] = this.rows;
         a[1] = this.columns;
         return a;
    }*/


}
