import java.util.Arrays;
import java.util.Scanner;

public class HMM1 {

	public static void main(String[] args) {

		
		Scanner sc = new Scanner(System.in);
		Matrix A = null;
		Matrix B = null;
		Matrix init_state = null;
		int[] observations = null;

        int lines = 0;
        do
        {
        	lines += 1;
        	String input = sc.nextLine();
        	String[] splitted = input.split("\\s+");
        	int k = 0;
        	int n = Integer.parseInt(splitted[k]);  // rows
        	int m;
        	k += 1;
        	switch (lines)
        	{
        		case 1:
                	m = Integer.parseInt(splitted[k]);  // columns	
                	k += 1;
        			A = parse_matrix(Arrays.copyOfRange(splitted, k, splitted.length) , n, m); 
        			break;
        		case 2:
                	m = Integer.parseInt(splitted[k]);  // columns	
                	k += 1;
        			B = parse_matrix(Arrays.copyOfRange(splitted, k, splitted.length) , n, m);
        			break;
        		case 3:        	
                	m = Integer.parseInt(splitted[k]);  // columns	
                	k += 1;
        			init_state = parse_matrix(Arrays.copyOfRange(splitted, k, splitted.length) , n, m);
        			break;
        		case 4:
        			String[] temp = Arrays.copyOfRange(splitted, 1, splitted.length);
        			observations = new int[temp.length];
        			for (int i=0; i<temp.length; i++)
        			{
        				observations[i] = Integer.parseInt(temp[i]);
        			}
        			break;
        		default:
        			System.err.println("Too many lines / matrices");
        	}
        }while (sc.hasNextLine());
        
		int p_len = init_state.number_of_columns();
		int N = observations.length;
		
		int obs_i = observations[0];
		
		Matrix alpha = new Matrix(p_len, N);
		
		Matrix init_state_t = init_state.transpose();
		
		Matrix alpha_0 = init_state_t.elementwise_multipy_with(B.get_column(obs_i));
		alpha.set_column_into_matrix(alpha_0, 0);
		
		for (int i=1; i<N; i++)
		{
			Matrix A_T = A.transpose();
			
			Matrix alpha_i = A_T.mat_multiply_with(alpha.get_column(i-1));
			
			obs_i = observations[i];
			
			alpha_i = alpha_i.elementwise_multipy_with(B.get_column(obs_i));
			alpha.set_column_into_matrix(alpha_i, i);			
		}
        
		double output = 0;
		for (int i=0; i<alpha.number_of_rows(); i++)
		{

			output += alpha.getElement(i, N-1);
		}
		
		System.out.println(output);
		
	}

	
	public static Matrix parse_matrix(String[] input, int rows, int columns)
	{
		Matrix matrix = new Matrix(rows, columns);
		int k=0;
		for (int i=0; i < rows; i++)
		{
			for (int j=0; j < columns; j++)
			{
				matrix.setElement(Double.parseDouble(input[k]), i, j);
				k += 1;
			}
		}
		return matrix;
	}
	
}
