import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class HMM3_beta {
	
	// Data
	public static Matrix A = null;
	public static Matrix B = null;
	public static Matrix pi = null;
	public static int[] observations = null;
	
	// a_pass
	public static Matrix alpha = null;
	public static Matrix c_t = null;
	
	// b_pass
	public static Matrix beta = null;
	
	// gammas
	public static Matrix gamma = null;
	public static List<Matrix> di_gammas = null;
	
	public static int N;
	public static int M;
	public static int T;
	
	public static double oldLogProb = Double.NEGATIVE_INFINITY;
	public static int iters = 0;
	public static int maxIters = 100;


	public static void main(String[] args) {

		
		Scanner sc = new Scanner(System.in);
        int lines = 0;
        do
        {
        	lines += 1;
        	String input = sc.nextLine();
        	String[] splitted = input.split("\\s+");
        	int k = 0;
        	int n = Integer.parseInt(splitted[k]);  // rows
        	int m;
        	k += 1;
        	switch (lines)
        	{
        		case 1:
                	m = Integer.parseInt(splitted[k]);  // columns	
                	k += 1;
        			A = parse_matrix(Arrays.copyOfRange(splitted, k, splitted.length) , n, m); 
        			break;
        		case 2:
                	m = Integer.parseInt(splitted[k]);  // columns	
                	k += 1;
        			B = parse_matrix(Arrays.copyOfRange(splitted, k, splitted.length) , n, m);
        			break;
        		case 3:        	
                	m = Integer.parseInt(splitted[k]);  // columns	
                	k += 1;
        			pi = parse_matrix(Arrays.copyOfRange(splitted, k, splitted.length) , n, m);
        			break;
        		case 4:
        			String[] temp = Arrays.copyOfRange(splitted, 1, splitted.length);
        			observations = new int[temp.length];
        			for (int i=0; i<temp.length; i++)
        			{
        				observations[i] = Integer.parseInt(temp[i]);
        			}
        			break;
        		default:
        			System.err.println("Too many lines / matrices");
        	}
        }while (sc.hasNextLine());

		N = pi.numOfColumns();
		M = B.numOfColumns();
		T = observations.length;
        
		iters = iters + 1;
		double logProb;

		while (iters < maxIters)
		{
			a_pass();  // alpha matrix and ct are correct!!!
			b_pass();  // beta matrix is correct!!!
			gammas();  // gamma matrix is
			estimate();
			logProb = compute_log();
			if (logProb > oldLogProb)
			{
				iters += 1;
				oldLogProb = logProb;
			}
			else
			{
				break;
			}
		}
		
		System.out.print(A.numOfRows());
		System.out.print(" ");
		System.out.print(A.numOfColumns());
		System.out.print(" ");
		for (int i=0; i<A.numOfRows(); i++)
		{
			for (int j=0; j<A.numOfColumns(); j++)
			{
				System.out.print(A.getElement(i, j));
				System.out.print(" ");
			}
		}
		System.out.println();
		System.out.print(B.numOfRows());
		System.out.print(" ");
		System.out.print(B.numOfColumns());
		System.out.print(" ");
		for (int i=0; i<B.numOfRows(); i++)
		{
			for (int j=0; j<B.numOfColumns(); j++)
			{
				System.out.print(B.getElement(i, j));
				System.out.print(" ");
			}
		}
		
	}

	
	public static double a_pass()
	{
		int obs_t = observations[0];
		
		double c0 = 0.0;
		alpha = new Matrix(N, T);
		c_t = new Matrix(T, 1);
		
		Matrix alpha_0 = new Matrix(N, 1);
		for (int i=0; i<N; i++)
		{
			double elem = pi.getElement(0, i) * B.getElement(i, obs_t);
			alpha_0.setElement(elem, i, 0);
			c0 += elem;
		}
		c_t.setElement(c0, 0, 0);
		alpha_0 = scale(c0, alpha_0, N);
		
		alpha.setColumn(alpha_0, 0);
		Matrix alpha_t = null;
		double ct = 0.0;
		for (int t=1; t<T; t++)
		{
			ct = 0.0;
			obs_t = observations[t];
			alpha_t = new Matrix(N, 1);
			for (int i=0; i<N; i++)
			{
				double a_t_i = 0.0;
				
				for (int j=0; j<N; j++)
				{
					a_t_i = a_t_i + alpha.getElement(j, t-1) * A.getElement(j, i);  // t-1 or obs_t-1?
				}
				a_t_i = a_t_i *  B.getElement(i, obs_t);
				alpha_t.setElement(a_t_i, i, 0);
				ct = ct + a_t_i;
			}
			ct = 1.0 / ct;
			c_t.setElement(ct, t, 0);
			alpha_t = scale(ct, alpha_t, N);
			alpha.setColumn(alpha_t, t);	
		}
		double output = 0;
		for (int i=0; i<N; i++)
		{

			output += alpha.getElement(i, T-1);
		}
		return output;		
	}
	
	public static void b_pass()
	{
		int obs_t = T-1; // observations[T-1];
		
		beta = new Matrix(N, T);
		for (int i=0; i<N; i++)
		{
			beta.setElement(c_t.getElement(obs_t, 0), i, obs_t);  // get 999 c_T so it is T-1 NOT obs_t
		}
		
		Matrix beta_t = null;
		for (int t=T-2; t>-1; t--)
		{
			obs_t = t; // observations[t];
			beta_t = new Matrix(N, 1);
			for (int i=0; i<N; i++)
			{
				double b_t_i = 0.0;
				int obs_t_1 = observations[t+1];
				for (int j=0; j<N; j++)
				{
					b_t_i = b_t_i + A.getElement(i, j) * B.getElement(j, obs_t_1) * beta.getElement(j, t+1);  // obs_t+1 or T+1 ???
				}
				b_t_i = c_t.getElement(t, 0) * b_t_i;
				beta_t.setElement(b_t_i, i, 0);
			}
			beta.setColumn(beta_t, t);
		}
	}
	
	public static void gammas()
	{
		di_gammas = new ArrayList<>();
		gamma = new Matrix(N, T);
		
		for(int t=0; t<T-1; t++)
		{
			double denom = 0.0;
			for (int i=0; i<N; i++)
			{
				for (int j=0; j<N; j++)
				{
					double alpha_t_i = alpha.getElement(i, t);
					
					double a_i_j = A.getElement(i, j);
					
					int obs_t_1 = observations[t+1];
					double b_j_O_t_1 = B.getElement(j, obs_t_1);  // maaaaaaaaybe obs_t_1, j
					
					double beta_t_1 = beta.getElement(j, t+1); // maaaaybe obs_t_1, j ORRR j, t+1
					
					denom = denom + alpha_t_i * a_i_j * b_j_O_t_1 * beta_t_1;	
				}
			}
			
			Matrix gamma_t = new Matrix(N, 1);
			Matrix di_gamma_t = new Matrix(N, N);
			for (int i=0; i<N; i++)
			{
				double gamma_t_i = 0.0;
				for (int j=0; j<N; j++)
				{
					double alpha_t_i = alpha.getElement(i, t);
					
					double a_i_j = A.getElement(i, j);
					
					int obs_t_1 = observations[t+1];
					double b_j_O_t_1 = B.getElement(j, obs_t_1);  // maaaaaaaaybe obs_t_1, j

					double beta_t_1 = beta.getElement(j, t+1); // maaaaybe obs_t_1, j ORRR j, t+1
					
					double res = alpha_t_i * a_i_j * b_j_O_t_1 * beta_t_1;	
					
					double di_gamma_i_t = res / denom;
					
					di_gamma_t.setElement(di_gamma_i_t, i, j);
					
					gamma_t_i = gamma_t_i + di_gamma_i_t;
				}
				gamma_t.setElement(gamma_t_i, i, 0);
				gamma.setColumn(gamma_t, t);
			}
			di_gammas.add(di_gamma_t);
		}

		double denom = 0;
		for (int i=0; i<N; i++)
		{
			denom = denom + alpha.getElement(i, T-1); 
		}
		for (int i=0; i<N; i++)
		{
			double elem = alpha.getElement(i, T-1) / denom;
			gamma.setElement(elem, i, T-1);
		}
	}
	
	public static void estimate()
	{
		// re-estimate pi
		for (int i=0; i<N; i++)
		{
			double g_0_i = gamma.getElement(i, 0);
			pi.setElement(g_0_i, 0, i);
		}
		
		// re-estimate A
		for (int i=0; i<N; i++)
		{
			for (int j=0; j<N; j++)
			{
				double numer = 0.0;
				double denom = 0.0;
				for (int t=0; t<T-1; t++)
				{
					numer = numer + di_gammas.get(t).getElement(i, j);
					denom = denom + gamma.getElement(i, t);
				}
				double elem = numer / denom;
				A.setElement(elem, i, j);
			}
		}
		
		// re-estimate B
		for (int i=0; i<N; i++)
		{
			for (int j=0; j<M; j++)
			{
				double numer = 0.0;
				double denom = 0.0;
				for (int t=0; t<T; t++)
				{
					int obs_t = observations[t];
					if (obs_t == j)
					{
						numer = numer + gamma.getElement(i, t);
					}
					denom = denom + gamma.getElement(i, t);
				}
				double elem = numer / denom;
				B.setElement(elem, i, j);  // fixed j,i to i,j
			}
		}
		
	}

	public static double compute_log()
	{
		double logProb = 0.0;
		for (int i=0; i<T-1; i++)
		{
			logProb = logProb + Math.log(c_t.getElement(i, 0));
		}
		logProb = - logProb;
		return logProb;
	}
		
	public static Matrix scale(double scaler, Matrix alpha_i, int N)
	{
		Matrix alpha_i_scaled = new Matrix(N, 1);
		
		for (int i=0; i<N; i++)
		{
			double elem_scaled = scaler * alpha_i.getElement(i, 0);
			alpha_i_scaled.setElement(elem_scaled, i, 0);
		}
		
		return alpha_i_scaled;
	}
		
	public static Matrix parse_matrix(String[] input, int rows, int columns)
	{
		Matrix matrix = new Matrix(rows, columns);
		int k=0;
		for (int i=0; i < rows; i++)
		{
			for (int j=0; j < columns; j++)
			{
				matrix.setElement(Double.parseDouble(input[k]), i, j);
				k += 1;
			}
		}
		return matrix;
	}
	
}
