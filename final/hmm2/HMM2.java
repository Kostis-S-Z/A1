import java.util.Arrays;
import java.util.Scanner;

public class HMM2 {

    public static void main(String[] args) {
        // don't care reading stuff from here untill
        Scanner sc = new Scanner(System.in);
        Matrix A = null;
        Matrix B = null;
        Matrix pi = null;
        Matrix observations = null;

        int lines = 0;
        do
        {
            lines += 1;
            String input = sc.nextLine();
            String[] splitted = input.split("\\s+");
            int k = 0;
            int n = Integer.parseInt(splitted[k]);  // rows
            int m;
            k += 1;
            switch (lines)
            {
                case 1:
                    m = Integer.parseInt(splitted[k]);  // columns
                    k += 1;
                    A = parse_matrix(Arrays.copyOfRange(splitted, k, splitted.length) , n, m);
                    break;
                case 2:
                    m = Integer.parseInt(splitted[k]);  // columns
                    k += 1;
                    B = parse_matrix(Arrays.copyOfRange(splitted, k, splitted.length) , n, m);
                    break;
                case 3:
                    m = Integer.parseInt(splitted[k]);  // columns
                    k += 1;
                    pi = parse_matrix(Arrays.copyOfRange(splitted, k, splitted.length) , n, m);
                    break;
                case 4:
                    String[] temp = Arrays.copyOfRange(splitted, 1, splitted.length);
                    observations = new Matrix(1, temp.length);
                    for (int i=0; i<temp.length; i++)
                    {
                        observations.setElement(Double.parseDouble(temp[i]), 0, i);
                    }
                    break;
                default:
                    System.err.println("Too many lines / matrices");
            }
        }while (sc.hasNextLine());
        // here reading stuff (end)

        int p_len = pi.numOfColumns();
        pi = pi.transpose();
        int N = observations.numOfColumns();
        Matrix init_delta = pi.elemMult(B.getColumn((int) observations.object[0][0]));
        Matrix delta = new Matrix(p_len, N);
        Matrix delta_max_idx = new Matrix(p_len, p_len * (N-1));         // alt col: N * (delta.columns-1)
        delta.setColumn(init_delta, 0); //*
        //System.out.printf("init delta is first column of following mat: %n");
        //delta.showMatrix();

        for (int i=1; i<N; i++) // because of *  old: int i=1; i<N; i++
        {
            Matrix last_delta_mat = delta.getColumn(i-1).transpose().repmat(p_len, 1);
            //System.out.printf("last_delta_mat: %n");
            //last_delta_mat.showMatrix();

            Matrix first_elem_mult = last_delta_mat.elemMult(A.transpose());

            Matrix cur_Obs_Mat = B.getColumn((int) observations.object[0][i]).repmat(1, p_len);
            //System.out.printf("cur_Obs_Mat: %n");
            //cur_Obs_Mat.showMatrix();

            Matrix second_elem_mult = first_elem_mult.elemMult(cur_Obs_Mat);

            Matrix max_rows = second_elem_mult.maxValRows();
            //System.out.printf("max_rows: %n");
            //max_rows.showMatrix();


            // fix for the probability = zero value special case!
            for (int n = 0; n < p_len; n++){
                if(max_rows.object[n][0] == 0){
                    for (int m = 1; m < p_len+1;m++){
                        max_rows.object[n][m] = 0;
                    }
                }
            }

            //System.out.printf("max_rows after zero fix: %n");
            //max_rows.showMatrix();

            //System.out.printf("Delta_max_idx size check %n");
            //delta_max_idx.showMatrix();

            // System.out.printf("i = %d of N-1 = %d: %n", i, N-1);
            for (int n = 0; n < p_len; n++)
                delta.object[n][i] = max_rows.object[n][0];
            for (int m = 0; m < A.columns; m++){        // olt boundary for m: m< p_len
                //System.out.printf("value of p_len %d, m %d, N %d: %n", p_len, m, N);
                for (int n = 0; n < p_len; n++) {
                    //System.out.printf("value of n %d and i %d of N=%d: %n", n, i, N);
                    //delta_max_idx.showMatrix();
                    //System.out.printf("Dim of delta_max_idx rows %d, columns %d %n", delta_max_idx.rows, delta_max_idx.columns);
                    //System.out.printf("Dim of max_rows rows %d, columns %d %n", max_rows.rows, max_rows.columns);
                    //System.out.printf("####### N = %d ########%n",N);
                    delta_max_idx.object[n][(i - 1) * p_len + m] = max_rows.object[n][m + 1]; // delta_max_idx.object[n][(i - 1) * "N-1 oder N" + m] = max_rows.object[n][m + 1];
                    //delta_max_idx.showMatrix();
                }
            }
            //System.out.printf("delta_mat: %n");
            //delta.showMatrix();

        }
        //System.out.printf("Delta_max_idx *** %n");
        //delta_max_idx.showMatrix();

        //System.out.printf("Delta_mat %n");
        //delta.showMatrix();

        Matrix last_column = new Matrix(p_len,1);
        last_column = last_column.getColumnPart(delta,N-1,0,p_len-1).transpose();
        //System.out.printf("Last column of Delta_mat transposed %n");
        //last_column.showMatrix();
        Matrix max_last_column = last_column.maxValRows();
        //System.out.printf("maxValRows applied onto last column of Delta_mat %n");
        //max_last_column.showMatrix();

/*        // braucht man das?
        // zero fix for last column max rows
        // fix for the probability = zero value special case!
        if(max_last_column.object[0][0] == 0){
            for (int m = 1; m < p_len+1;m++){
                max_last_column.object[0][m] = 0;
            }
        }*/


        //System.out.printf("maxValRows applied onto last column of Delta_mat after zero fix %n");
        //max_last_column.showMatrix();


        int col = 0;
        boolean flag = false;
        Matrix X_Seq = new Matrix(N,1);

        //Attention from here, the states number are shiftet by +1 former: A = 0, now A=1
        for (int m=1; m < max_last_column.columns; m++) {
            if (max_last_column.object[0][m] == 1) {
                if (flag == true) {
                    X_Seq = X_Seq.addColumn();
                    col = col + 1;
                    X_Seq.object[0][col] = m;
                } else
                    X_Seq.object[0][col] = m;
                flag = true;
            }
            //System.out.printf("X_Seq in run nr.: %d %n", m);
            //X_Seq.showMatrix();
        }

        int length = N;
        boolean flag2 = false;
        int row_state = 0;
        //  System.out.printf("The most proper (with equal probability) states are: %n"); // Attention, 0 0 0 0 means A A 0 0.   *

        X_Seq = Possible_Pathes(X_Seq, delta_max_idx, p_len, length, row_state, flag2);
        // X_Seq has included all pathes, just choose any column and get a result for Kattis.
        //X_Seq.getColumn(0).showMatrix();

        // adjust Output to Kattis
        for (int n = 0; n < X_Seq.rows; n++){
            int value = (int) X_Seq.object[X_Seq.rows-1 - n][0]-1;
            System.out.printf("%d ", value);
        }
    }

    public static Matrix Possible_Pathes(Matrix X_Seq, Matrix delta_max_idx, int p_length, int length, int row_state, boolean flag) {
        int T = X_Seq.columns;
        //System.out.printf("Number of sequence columns T: %d %n", T);
        int zeile;
        int spalte;
        //    boolean zero_poss_mat = false;

        //System.out.printf("p_lengt: %d %n", p_length);
        //System.out.printf("length: %d %n", length);
        Matrix tmp_col_vec; // = new Matrix(p_length, 1);
        for (int t = 0; t < T; t++) {
            //System.out.printf("t von T: %d von ", t);
            //System.out.printf("%d %n", T - 1);
            for (int m = 0; m < p_length; m++) {
                zeile = (int) X_Seq.object[row_state][t]-1;
                //System.out.printf("Zeile ist: %d %n", zeile);
                spalte = (length - 1) * p_length - m - 1;          // old spalte = (length - 1) * p_length - m - 1;
                //System.out.printf("Spalte ist: %d %n", spalte);
                //System.out.printf("delta_max_idx_ is: %n");
                //delta_max_idx.showMatrix();
                //System.out.printf("m =: %d %n", m);
                //System.out.printf("Wert von delta_max_idx.object[ zeile ][ spalte ] ist: %d %n", (int) delta_max_idx.object[0][0]);
                //               System.out.printf("Wert von delta_max_idx.object[ zeile+1 ][ spalte ] ist: %d %n", (int)delta_max_idx.object[ zeile+1 ][ spalte ]);
//                System.out.printf("Wert von delta_max_idx.object[ zeile+2 ][ spalte ] ist: %d %n", (int)delta_max_idx.object[ zeile+2 ][ spalte ]);
                //delta_max_idx.showMatrix();

                //      if (zeile == -1 || spalte == -1)
                //          zero_poss_mat = true;

                //    if (zero_poss_mat == false) {

                if (delta_max_idx.object[zeile][spalte] == 1) {
                    //System.out.println("Value of flag before it is checked");
                    //System.out.println(flag);
                    if (flag == false) {
                        X_Seq.object[row_state + 1][t] = p_length - m;
                        //System.out.printf("*****X_Seq***** %n");
                        //X_Seq.showMatrix();
                        flag = true;
                    } else {
                        X_Seq = X_Seq.addColumn();
                        tmp_col_vec = X_Seq.getColumn(t);
                        X_Seq = X_Seq.set_column_into_matrix(tmp_col_vec, X_Seq.columns - 1);
                        X_Seq.object[row_state + 1][X_Seq.columns - 1] = p_length - m;
                        if (m == p_length - 1) {
                            flag = false;
                        }
                    }
                }
            }
            flag = false;
        }
        //   }
        // if (zero_poss_mat == false) {
        row_state = row_state + 1;
        length = length - 1;
        //System.out.printf("X_Seq: %n");
        //X_Seq.showMatrix();
        if (length == 1) {
            return X_Seq;
        } else {
            return Possible_Pathes(X_Seq, delta_max_idx, p_length, length, row_state, flag);
        }
    }
    // else{
    //     return X_Seq;
    // }
    //}
    // don't care, reading stuff
    public static Matrix parse_matrix(String[] input, int rows, int columns)
    {
        Matrix matrix = new Matrix(rows, columns);
        int k=0;
        for (int i=0; i < rows; i++)
        {
            for (int j=0; j < columns; j++)
            {
                matrix.setElement(Double.parseDouble(input[k]), i, j);
                k += 1;
            }
        }
        return matrix;
    }
    // don't care, reading stuff
}