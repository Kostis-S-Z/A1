public class Matrix {

    public int rows;
    public int columns;
    public double[][] object;

    public Matrix(double[][] data) {
        /*
         * Create Matrix from given 2-d array
         */
        this.rows = data.length;
        this.columns = data[0].length;
        this.object = new double[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++)
                object[i][j] = data[i][j];
        }
    }

    public Matrix(int row, int col) {
        /*
         * Initialize Matrix given number of rows and columns
         */
        this.rows = row;
        this.columns = col;
        this.object = new double[rows][columns];
    }

    // Method to set an element in row n and col m
    public Matrix setElement(double elem, int row, int col) {
       this.object[row][col] = elem;
       return this;
    }

    // Method to get an element in row n and col m
    public double getElement(int row, int col) {
        return this.object[row][col];
    }

    public int getElementInt(int row, int col) {
        return (int) this.object[row][col];
    }

    // Method to get a part of a row from a Matrix
    public Matrix getRowPart(Matrix A, int row, int start_col, int end_col) {
        for (int m = start_col; m < end_col + 1; m++) {
            this.object[0][m - start_col] = A.object[row][m];
        }
        return this;
    }

    // Method to get a part of a column from a Matrix
    public Matrix getColumnPart(Matrix A, int col, int start_row, int end_row) {
        for (int n = start_row; n < end_row + 1; n++) {
            this.object[n - start_row][0] = A.object[n][col];
        }
        return this;
    }

    // a method which can write a vector to a matrix like: "alpha_Mat(:,1) = vector"
    public Matrix setColumn(Matrix column, int index) {
        for (int n = 0; n < this.rows; n++)
            this.object[n][index] = column.object[n][0];
        return this;
    }

    // equal to setColumn
    // a method which can write a vector to a matrix like: "alpha_Mat(:,1) = vector"
    public Matrix set_column_into_matrix(Matrix vec, int col) {
        for (int n = 0; n < this.rows; n++)
            this.object[n][col] = vec.object[n][0];
        return this;
    }

    public int numOfRows() {
        return this.object.length;
    }

    public int numOfColumns() {
        return this.object[0].length;
    }

    public void showMatrix() {
        /*
         * Print Matrix
         */
        for (int i = 0; i < rows; i++) {
            String print_row = "";
            for (int j = 0; j < columns; j++) {
                print_row = print_row + object[i][j] + " ";
            }
            System.out.println(print_row);
        }
    }

    public Matrix dot(Matrix B) {
        /*
         * Matrix multiplication with a given Matrix
         */
        Matrix A = this;
        if (A.columns != B.rows)
            throw new RuntimeException("Multiplication not possible... check rows and columns of the matrices!");
        Matrix Result = new Matrix(A.numOfRows(), B.numOfColumns());
        for (int i = 0; i < Result.rows; i++)
            for (int j = 0; j < Result.columns; j++)
                for (int k = 0; k < A.columns; k++)
                    Result.object[i][j] = Result.object[i][j] + (A.object[i][k] * B.object[k][j]);
        return Result;
    }

    public Matrix getRow(int row_idx) {
        /*
         * Returns a specific row of the Matrix
         */
        Matrix row = new Matrix(1, this.numOfColumns());
        for (int i = 0; i < row.numOfColumns(); i++)
            row.object[0][i] = this.object[row_idx][i];
        return row;
    }

    public Matrix getColumn(int column_idx) {
        /*
         * Return a specific column of the Matrix
         */
        Matrix column = new Matrix(this.numOfRows(), 1);
        for (int i = 0; i < column.numOfRows(); i++)
            column.object[i][0] = this.object[i][column_idx];
        return column;
    }

    public Matrix elemMult(Matrix B) {
        /*
         * Multiplication of elements of two Matrices
         */
        Matrix A = this;
        if (A.columns != B.columns || A.rows != B.rows)
            throw new RuntimeException("For pointwise multiplication the sizes of the matrices must be equal");
        Matrix Result = new Matrix(A.numOfRows(), B.numOfColumns());
        for (int i = 0; i < Result.rows; i++)
            for (int j = 0; j < Result.columns; j++)
                Result.object[i][j] = A.object[i][j] * B.object[i][j];
        return Result;

    }

    public Matrix transpose() {
        /*
         * Transpose the Matrix
         */
        Matrix transposed = new Matrix(this.numOfColumns(), this.numOfRows());
        for (int n = 0; n < this.numOfRows(); n++)
            for (int m = 0; m < numOfColumns(); m++)
                transposed.object[m][n] = this.object[n][m];
        return transposed;
    }

    public Matrix repmat(int y_way, int x_way) {
        // create output matrix (empty)
        Matrix rep_result = new Matrix(this.rows * y_way, this.columns * x_way);
        for (int y = 0; y < y_way; y++)
            for (int n = 0; n < this.rows; n++)
                for (int x = 0; x < x_way; x++)
                    for (int m = 0; m < this.columns; m++)
                        rep_result.object[y * this.rows + n][x * this.columns + m] = this.object[n][m];
        return rep_result;
    }

/*  doesn't work!  // a method, that cast the matrix elements to int
    public Matrix castToInt(){
        Matrix Int_mat = new Matrix(this.rows, this.columns);
        for (int n=0; n<this.rows; n++) {
            for (int m = 0; m < this.columns; m++) {
                Int_mat.object[n][m] = (int) this.object[n][m];
            }
        }
        return Int_mat;
    }*/

    // a method which add a column at the end of a matrix
    public Matrix addColumn() {
        Matrix new_mat = new Matrix(this.rows, this.columns + 1);
        for (int m = 0; m < this.columns; m++) {
            Matrix current_column = this.getColumn(m);
            new_mat.setColumn(current_column, m);
        }
        return new_mat;
    }

    public Matrix maxValRows() {
        /*
         * A method which find the max values in a row and its index.
         *
         */
        Matrix max_of_row_val_vec = new Matrix(this.rows, 1);
        Matrix idx_of_max_mat = new Matrix(this.rows, this.columns);
        double current_max_in_row;
        for (int n = 0; n < this.rows; n++) {
            current_max_in_row = this.object[n][0];
            idx_of_max_mat.object[n][0] = 1;
            for (int m = 0; m < this.columns; m++) {
                if (this.object[n][m] > current_max_in_row) {
                    // Value stuff
                    current_max_in_row = this.object[n][m];
                    //Index Stuff
                    idx_of_max_mat.object[n][m] = 1;
                    for (int prev_m = 0; prev_m < m; prev_m++)
                        idx_of_max_mat.object[n][prev_m] = 0;
                } else if (this.object[n][m] == current_max_in_row) {
                    idx_of_max_mat.object[n][m] = 1;
                }
                max_of_row_val_vec.object[n][0] = current_max_in_row;
            }
        }
        Matrix Output = new Matrix(this.rows, this.columns + 1);
        Output = Output.setColumn(max_of_row_val_vec, 0);
        Matrix idx_column; // = new matrix(this.rows,1);
        for (int m = 1; m < Output.columns; m++) {
            idx_column = idx_of_max_mat.getColumn(m - 1);
            Output = Output.setColumn(idx_column, m);
        }
        return Output;
    }

    // Method to multiply Matrix with a factor:
    public Matrix multiplyWithFactor(double factor){
        for (int n=0; n< this.rows; n++){
            for (int m=0; m< this.columns; m++){
                this.object[n][m] = factor * this.object[n][m];
            }
        }
        return this;
    }


}